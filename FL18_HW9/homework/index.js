const one = 1;
const five = 5;
const two = 2;
const three = 3;
const sixtyHundred = 60000;
const minusOne = -1;

class Magazine {
    constructor() {
        this.states = [
            'ReadyForPushNotification',
            'ReadyForApprove',
            'ReadyForPublish',
            'PublishInProgress'
        ];
        this.articles = [];
        this.followers = [];
        this.staff = [];

        this.current = this.states[0];
    }
    sendNotificationsToFollowers(){
        this.articles.forEach((a) => {
            this.followers.forEach((f) => {
                if (f.topic === a.type) {
                    f.onUpdate(a.article)
                }
            })
        })
    }
}

class MagazineEmployee {
    constructor(name, type, magazine) {
        this.name = name;
        this.type = type;
        this.magazine = magazine;
        this.magazine.staff.push(this);
    }

    addArticle(article) {
        this.article = article;
        if (this.type !== 'manager') {
            this.magazine.articles.push({article: this.article, type: this.type});
        }
        if (this.magazine.articles.length >= five) {
            this.magazine.current = this.magazine.states[one];
        }
    }

    approve() {
        if (
            this.magazine.current === 'ReadyForApprove' &&
            this.type === 'manager'
        ) {
            console.log(`Hello ${this.name}. You've approved the changes`);
            this.magazine.current = this.magazine.states[two];
        } else if (this.magazine.articles.length < five && this.type === 'manager') {
            console.log(`Hello ${this.name}. You can't approve. We don't have enough of publications`);
        } else if (
            this.magazine.current === 'ReadyForPublish' &&
            this.type === 'manager'
        ) {
            console.log(`Hello ${this.name}. Publications have already been approved by you.`);
        } else if (this.magazine.current === 'PublishInProgress') {
            if (this.type !== 'manager'){
                console.log('You do not have permissions to do it');
            }else{
            console.log(`Hello ${this.name}. While we are publishing we can't do any actions`);
            }
        } else {
            console.log('You do not have permissions to do it');
        }
    }

    publish() {
        if (this.magazine.current === 'ReadyForPushNotification') {
            console.log(`Hello ${this.name}.  You can't publish. We are creating publications now.`);
        }else if (this.magazine.current === 'ReadyForApprove') {
            console.log(`Hello ${this.name} You can't publish. We don't have a manager's approval.`)
        }else if(this.magazine.current === 'ReadyForPublish') {
            console.log(`Hello ${this.name}. You've recently published publications.`);
            this.magazine.sendNotificationsToFollowers();
            this.magazine.current = this.magazine.states[three];
            setTimeout(() => {
                this.magazine.current = this.magazine.states[0];
                this.magazine.articles = [];
                console.log(this.magazine);
            }, sixtyHundred);
        } else if (this.magazine.current === 'PublishInProgress') {
            console.log(`Hello ${this.name}. While we are publishing we can't do any actions`);
        }
    }
}

class Follower {
    constructor(name) {
        this.name = name;
    }

    subscribeTo(magazine, topic) {
        this.magazine = magazine;
        this.topic = topic;
        this.magazine.followers.push(this);
    }

    unsubscribe() {
        const index = this.magazine.followers.indexOf(this);
        if (index !== minusOne) {
            this.magazine.followers.splice(index, one);
        }
    }

    onUpdate(data) {
        console.log(`${data} ${this.name}`)
        } 
}
