const connection = new WebSocket('ws://localhost:8080');

const ten = 10;

connection.onopen = () => {
  console.log('connected');
  connection.userName = prompt('Enter your name: ')
};

connection.onclose = () => {
  console.error('disconnected');
};

connection.onerror = error => {
  console.error('failed to connect', error);
};

connection.onmessage = event => {
  console.log('received', event.data);
  let li = document.createElement('li');
  li.innerText = event.data;
  document.querySelector('#chat').append(li);
};

document.querySelector('form').addEventListener('submit', event => {
  event.preventDefault();
  let message = document.querySelector('#message').value;
  let d = new Date()
let h = d.getHours()
let m = d.getMinutes()
let s = d.getSeconds()
let temp1 = (m < ten ? '0' : '') + m;
let temp2 = (s < ten ? '0' : '') + s;

let displayDate = h + ':' + temp1 + ':' + temp2

  connection.send(`${connection.userName}: ${message}\n ${displayDate}`);
  document.querySelector('#message').value = '';
});
