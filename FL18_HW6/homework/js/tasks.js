function getMaxEvenElement(arr) {
  const filterEven = arr.reduce((prev, curr) => {
    let evenCheck = 2
    if (Number(curr) % evenCheck === 0) {
      prev.push(Number(curr))
    }
    return [...prev];
  }, []);
  return Math.max(...filterEven);
}

 let a = 3;
 let b = 5;

 [a, b] = [b, a]

 function getValue(value){
     return value ?? '-'
 }

 function getObjFromArray(array){
    return Object.fromEntries(new Map(array));
 }

 function addUniqueId(obj) {
  return {
      id: Symbol(),
      ...obj
  }   
}

function regroupObject({name: firstName, details: {id, age, university } }) {
  return {
       university,
       user: {
           age,
           firstName,
           id
       }
 };
}

const getArrayWithUniqueElements = arr => [...new Set(arr)];

function hideNumber(phoneNumber){
  let showDigital = -4;
  let fullNumber = 10;
  return phoneNumber.slice(showDigital).padStart(fullNumber, '*');
}

function required() {
  throw new Error('Missing property');
}

function add(x = required(), y = required()) {
   return x + y;
}

function* generateIterableSequence(array = ['I', 'love', 'EPAM']) {
  for (let word of array){
  yield word  
  }
}
