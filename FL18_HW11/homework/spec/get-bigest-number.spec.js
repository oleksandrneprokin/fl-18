// Your code goes here
let myLocalVariable2 = require('../src/get-bigest-number');

describe('CommonJS modules', () => {
    it('should return not enough arguments', async () => {
        expect(function (){
        myLocalVariable2([3])
      }).toThrowError('Not enough arguments')
    });
    it('should return too many arguments', async () => {
      expect(function (){
      myLocalVariable2([5, 7, 9, 10, 12, 14, 35, 53, 2, 41, 11])
    }).toThrowError('Too many arguments')
  });
    it('should return wrong argument type', async () => {
    expect(function (){
    myLocalVariable2(['hard', 'work'])
  }).toThrowError('Wrong argument type')
  });
    it('should return the biggest value', async () => {
    expect(myLocalVariable2([5, 9, 15])).toBe(15)
  });
  });
