// Your code goes here
let getBigestNumber = function(arr){
    if (arr.length < 2) {
        throw new Error('Not enough arguments')
    } else if (arr.length > 10){
        throw new Error('Too many arguments')
    } else if (arr.every( function(element){
        if (typeof element === 'number') {
          return true;
      } else {
          return false;
      }
  }) === false){
    throw new Error('Wrong argument type')
      }else{
    return Math.max.apply(null, arr);
}
}

module.exports = getBigestNumber;