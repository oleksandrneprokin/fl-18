// Write your code here
const minusOne = -1;

$(document).ready(function() {
    let $keys = $('.button');
    let $total = $('.screen');
    
    let operators = ['+', '-', '/', '*'];

    function calculateString(str) {
        return new Function('return ' + str)();
      }
      
     $keys.click(function() {  
        let keyVal = $(this).data('val');
        let output = $total.html();
        let lastChar = output[output.length - 1];
               
        if (keyVal === 'clear') {
            $total.html('0');      
           } else if (keyVal === '=') {
            if (calculateString(output) === Infinity){
                $total.html('ERROR')
            } else{                       
                $total.html(output.startsWith('0') ? calculateString(output.slice(1)) : calculateString(output));
            
                $total.addClass('complete');

                let $result = $('<p class="log">' + '<span class="circle">' + '&nbsp;' 
                + '&nbsp;' + '&nbsp;' + '</span>' + '&nbsp;' + output + '=' 
                + (output.startsWith('0') ? calculateString(output.slice(1)) : calculateString(output)) + '&nbsp;' 
                + '<span class="delete">' + 'X' + '</span>' + '</p>');

                $( 'p:contains(48)' ).css( 'border-bottom', '1px solid black');

                      
                $('.logs').prepend($result);
                
                
                  $('.delete').click(function(){
                     $(this).parent('p').remove();
                    });

                    $('.circle').on('click', function(e){  
                        $(e.target).toggleClass( 'red-background' );
                      });

                    $('.logs').scroll(function(){
                        console.log(`Scroll Top: ${$('.logs').scrollTop()}`);
                    });

            }                                
        } else if ($(this).parent().parent().parent().is('.operators')) {
            if ($total.is('.complete')) {
                $total.removeClass('complete');
            }
            if (output !== '' && operators.indexOf(lastChar) === minusOne) {
                $total.html($total.html() + keyVal);
            } else if (output === '' && keyVal === '-') {
                $total.html($total.html() + keyVal);
            }            
        }else {
            if ($total.is('.complete')) {
            $total.html(keyVal);
            $total.removeClass('complete');
            } else {
                $total.html($total.html() + keyVal);
            }
        }
    });

});